"use strict";

const tabs = document.querySelectorAll(".tabs-title");
const tabsArray = [...tabs];
const texts = document.querySelector(".tabs-content").children;
const textsArray = [...texts];

function setDefault() {
  tabsArray.forEach((element) => {
    element.classList.remove("active");

    textsArray.forEach((element) => {
      element.style.display = "none";
    });
  });
}
setDefault();

// function createData(textsArray) {
//   textsArray.forEach((text) => {
//     if (!text.dataset.name) {
//       text.dataset.name = "none";
//     }
//   });
// }
// createData(textsArray);

// function newData(textsArray, tabsArray) {
//   tabsArray.forEach((tab) => {
//     textsArray.forEach((text) => {
//       if (tab.innerHTML !== text.dataset.name) {
//         console.log(text.dataset.name);
//       }
//     });
//   });
// }
// newData(textsArray, tabsArray);

tabsArray.forEach((tab) => {
  tab.addEventListener("click", () => {
    textsArray.forEach((text) => {
      if (tab.innerHTML === text.dataset.name) {
        setDefault();
        tab.classList.add("active");
        text.style.display = "block";
      }
    });
  });
});
